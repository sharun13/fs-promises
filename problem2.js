const fs = require('fs');
const folderPath = __dirname + '/data/';

function textManipulator() {

    let nameFile = "FileNames.txt";

    function readFile() {

        return new Promise((resolve, reject) => {

            let lipsumPath = folderPath + 'lipsum.txt';
            fs.readFile(lipsumPath, 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });

        });

    }

    function convertToUpperCase(data) {
        let upperCasePromise = new Promise((resolve, reject) => {
            let upperCaseFile = data.toUpperCase();
            let upperCaseFileName = "upperCaseFile.txt";
            let upperCaseFilePath = folderPath + upperCaseFileName;
            fs.writeFile(upperCaseFilePath, upperCaseFile, (err) => {
                if (err) {
                    reject(err);
                } else {
                    writeFileNameToStorage(upperCaseFileName);
                    resolve(upperCaseFilePath);
                }
            });
        });

        return upperCasePromise;

    }

    function convertToLowerCase(filePath) {

        let lowerCasePromise = new Promise((resolve, reject) => {

            fs.readFile(filePath, 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    let lowerCaseFile = data.toLowerCase()
                        .split(' ');
                    let joinedLowerCaseText = lowerCaseFile.join('\n');
                    let lowerCaseFileName = "splitToLowerCaseFile.txt";
                    let lowerCaseFilePath = folderPath + lowerCaseFileName;
                    fs.writeFile(lowerCaseFilePath, joinedLowerCaseText, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            writeFileNameToStorage(lowerCaseFileName);
                            resolve(lowerCaseFilePath);
                        }
                    });
                }
            });
        });

        return lowerCasePromise;

    }

    function sortingFunction(filePath) {

        let sortingPromise = new Promise((resolve, reject) => {

            fs.readFile(filePath, 'utf-8', (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    let sortedFile = data.split('\n')
                        .sort()
                        .join(' ');
                    let sortedFileName = "sortedFile.txt";
                    let sortedFilePath = folderPath + sortedFileName;
                    fs.writeFile(sortedFilePath, sortedFile, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(sortedFileName);
                        }
                    });
                }
            });
        });

        return sortingPromise;

    }

    function writeFileNameToStorage(fileName) {
        let filePath = folderPath + nameFile;
        fs.appendFile(filePath, (fileName + " "), function (err) {
            if (err) {
                console.log(err);
            } else {
                console.log(`Updated file names with ${fileName}`);
            }
        });
    }

    function deleteAllFiles() {
        let nameFilePath = folderPath + nameFile;
        fs.readFile(nameFilePath, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                data = data.trim();
                let filesToBeDeleted = data.split(' ');
                let filesDeleted = filesToBeDeleted.map((file) => {
                    let deleteFilePath = folderPath + file;
                    fs.unlink(deleteFilePath, (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log(`${file} deleted successfully`);
                        }
                    })
                });
            }
        });
    }

    readFile()
        .then((result) => {
            console.log("lipsum.txt file has been read");
            return convertToUpperCase(result);
        })
        .then((upperCaseFilePath) => {
            console.log("Upper case file has been created");
            return convertToLowerCase(upperCaseFilePath);
        })
        .then((lowerCaseFilePath) => {
            console.log("split lower case file has been created");
            return sortingFunction(lowerCaseFilePath);
        })
        .then((sortedFileName) => {
            console.log("Text has been sorted");
            writeFileNameToStorage(sortedFileName);
            setTimeout(() => {
                deleteAllFiles();
            }, 3000);
        })
        .catch((err) => {
            console.log(err);
        });

}

module.exports = textManipulator;