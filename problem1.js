const fs = require('fs');
const { resolve } = require('path');

function createAndDeleteFiles(directory, countOfFiles) {

    directory = __dirname + '/' + directory;

    function createDirectory(directory) {
        return new Promise(function (resolve, reject) {
            fs.mkdir((directory), (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve("Directory created");
                }
            });
        });
    }

    function createFiles(directory, countOfFiles) {

        let createFilePromise = new Promise((resolve, reject) => {
            let count = 1;
            const intervalID = setInterval(() => {
                if (count < countOfFiles) {
                    let file = "/fileNumber" + count + ".json";
                    let filePath = directory + file;
                    fs.writeFile(filePath, `${file} has been created`, (err) => {
                        if (err) {
                            reject(err);
                        } else {
                            resolve("Files created");
                        }
                    });
                } else {
                    clearInterval(intervalID);
                }
                count++;
            }, 1000);
        });

        return createFilePromise;

    }

    function deleteFiles(directory) {
        fs.readdir(directory, (err, data) => {
            if (err) {
                console.log(err);
            }
            let deletedFiles = data.map((file) => {
                let fileName = directory + '/' + file;
                fs.unlink(fileName, (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
                return true;
            });
        });
    }

    createDirectory(directory)
        .then((result) => {
            console.log(result);
            return createFiles(directory, countOfFiles);
        })
        .then((result) => {
            console.log(result);
            setTimeout(() => {
                deleteFiles(directory);
                console.log("Files Deleted");
            }, 6000);
        })
        .catch((err) => {
            console.log(err);
        });

}

module.exports = createAndDeleteFiles;