const createAndDeleteFiles = require('../problem1')

const directory = 'randomFileFolder';
const countOfFiles = Math.ceil(Math.random() * 5);

createAndDeleteFiles(directory, countOfFiles);